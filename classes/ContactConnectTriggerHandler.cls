/**
 * Created by User on 1/23/2019.
 */

public with sharing class ContactConnectTriggerHandler {
    public static void linkToAccount(List<Contact> contacts) {
        if (contacts.isEmpty()) {
            return;
        }
        Set<String> relatedAccountID = new Set<String>();
        for (Contact contact : [SELECT AccountId FROM Contact]) {
            relatedAccountID.add(contact.AccountId);
        }
        Map<String,Account> accounts = new Map<String, Account>([SELECT Activity__c FROM Account WHERE (Id NOT IN :relatedAccountID) AND (Activity__c != null)]);
        List<Date> dates = new List<Date>();
        Date avgDate;
        String accountId = '';
        Integer tempIndex = 0;
        for (Account account : accounts.values()) {
            dates.add(account.Activity__c);
        }
        if (dates.isEmpty()) {
            return;
        }
        dates.sort();
        avgDate = dates[0].addDays(dates[0].daysBetween(dates[dates.size()-1]) / 2);
        for (Contact contact : contacts) {
            if (accounts.size() == 0) {
                contact.isAccountSet__c = false;
                continue;
            }
            accountId = accounts.values()[0].id;
            for (Integer i = 1; i < accounts.size(); i++) {
                if (Math.abs(accounts.values()[i].Activity__c.daysBetween(avgDate)) <= Math.abs(accounts.values()[i - 1].Activity__c.daysBetween(avgDate))) {
                    accountId = accounts.values()[i].id;
                }
            }
            contact.AccountId = accountId;
            contact.isAccountSet__c = true;
            accounts.remove(accountId);
        }
    }
}