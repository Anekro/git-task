/**
 * Created by User on 1/17/2019.
 */

public abstract with sharing class TypeClass {
    protected String contactType;
    protected Double amountOfMoney;
    public static final string TYPE_PREMIER = 'Premier';
    public static final string TYPE_PERSON = 'Person';
    public String getContactType() {
        return contactType;
    }
}