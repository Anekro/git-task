/**
 * Created by User on 1/17/2019.
 */

public with sharing class PublicContact extends TypeClass {
    Integer volunteerNumber;
    Contact contactRecord;
    public PublicContact(Contact contact, integer volunteerNumber) {
        this.contactRecord   = contact;
        this.volunteerNumber = volunteerNumber;
        contactType = TYPE_PERSON;
    }
    public Integer getVolunteerNumber() {
        return volunteerNumber;
    }
    public Contact getContactRecord() {
        return contactRecord;
    }
}