/**
 * Created by User on 1/17/2019.
 */

public with sharing class AccountSwitcherImpl implements AccountSwitcher  {
    public static void methodA() {
        System.debug('This is method A!');
    }
    public static void methodB() {
        System.debug('This is method B!');
    }
    public static void methodC() {
        System.debug('This is method C!');
    }
    public static List<TypeClass> allContacts = new List<TypeClass>();
    public class DataResult {
        public List<PrivateContact> changedPrivateContacts;
        public List<PublicContact>  changedPublicContacts;
        public List<PrivateContact> nonChangedPrivateContacts;
        public DataResult() {
            changedPrivateContacts      = new List<PrivateContact>();
            changedPublicContacts       = new List<PublicContact>();
            nonChangedPrivateContacts   = new List<PrivateContact>();
        }
    }
    public static void getAndSort() {
        Integer tempRandomNumber;
        for (Contact contact : [SELECT Amount__c, AccountId FROM Contact]) {
            tempRandomNumber = createRandom6xCode();
            if (contact.Amount__c != null) {
                if (contact.Amount__c > 10000.0) {
                    allContacts.add(
                            new PrivateContact(
                                    contact,
                                    tempRandomNumber
                            )
                    );
                }
                else {
                    allContacts.add(
                            new PublicContact(
                                    contact,
                                    Math.mod(tempRandomNumber,2) == 1 ?
                                            tempRandomNumber - 1
                                            : tempRandomNumber
                            )
                    );
                }
            }
        }
    }
    public static DataResult switchAccount() {
        DataResult result = new DataResult();
        List<PrivateContact> privateContacts = new List<PrivateContact>();
        List<PublicContact> publicContacts  = new List<PublicContact>();
        List<Contact> contactToUpdate = new List<Contact>();
        String AccId = '';
        Integer minusIndex = 0;
        for (TypeClass contact : allContacts) {
            if (contact instanceof PrivateContact) {
                privateContacts.add((PrivateContact)contact);
            }
            else {
                publicContacts.add((PublicContact)contact);
            }
        }
        for (Integer i = 0; i < Math.min(publicContacts.size(),privateContacts.size()); i++) {
            if (privateContacts[i].getContactType() == TypeClass.TYPE_PREMIER ) {
                result.nonChangedPrivateContacts.add(privateContacts[i]);
                minusIndex++;
                continue;
            }
            AccId = privateContacts[i].getContactRecord().AccountId;
            privateContacts[i].getContactRecord().AccountId = publicContacts[i - minusIndex].getContactRecord().AccountId;
            publicContacts[i - minusIndex].getContactRecord().AccountId = AccId;
            contactToUpdate.add(publicContacts[i - minusIndex].getContactRecord());
            contactToUpdate.add(privateContacts[i].getContactRecord());
            result.changedPrivateContacts.add(privateContacts[i]);
            result.changedPublicContacts.add(publicContacts[i - minusIndex]);
        }
        try {
            if (!contactToUpdate.isEmpty()) {
                update contactToUpdate;
            }
        } catch (Exception e) {
            System.debug('Error in ' + e.getStackTraceString() +  '. Message: ' + e.getMessage());
        }
        return result;
    }
    static Integer createRandom6xCode() {
        Integer result = Math.round(Math.random()*1000000);
        String stringResult = String.valueOf(result);
        while (stringResult.length() < 6) {
            stringResult = '0' + stringResult;
        }
        return Integer.valueOf(stringResult);
    }
}