/**
 * Created by User on 1/17/2019.
 */

public with sharing class PrivateContact extends TypeClass {
    Integer cardNumber;
    Contact contactRecord;
    public PrivateContact(Contact contact, integer cardNumber) {
        this.contactRecord = contact;
        this.cardNumber    = cardNumber;
        for (String charString : String.valueOf(cardNumber).split('')) {
            if (String.valueOf(cardNumber).countMatches(charString) > 2) {
                contactType = TYPE_PREMIER;
                break;
            }
        }
        if (String.isBlank(contactType)) {
            contactType = TYPE_PERSON;
        }
    }
    public Integer getCardNumber() {
        return cardNumber;
    }
    public Contact getContactRecord() {
        return contactRecord;
    }
}