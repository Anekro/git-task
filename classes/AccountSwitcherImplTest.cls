/**
 * Created by User on 1/18/2019.
 */

@IsTest
private class AccountSwitcherImplTest {
    @isTest
    public static void testGetAndSort() {
        getTestData();
        insert new Contact(LastName = 'Fake');
        AccountSwitcherImpl.getAndSort();
        Integer amountOfContacts = [SELECT count() FROM Contact];
        System.assert(AccountSwitcherImpl.allContacts.size() != amountOfContacts);
        System.assert(AccountSwitcherImpl.allContacts.size() != amountOfContacts);
    }
    @isTest
    public static void testSwitchAccount() {
        getSwitchTestData();
        PrivateContact tstContact = (PrivateContact)AccountSwitcherImpl.allContacts[6];
        String idString = tstContact.getContactRecord().AccountId;
        AccountSwitcherImpl.DataResult result = AccountSwitcherImpl.switchAccount();
        System.assert(result.nonChangedPrivateContacts.size() == 1);
        System.assertEquals(result.changedPublicContacts[0].getContactRecord().AccountId, idString);
    }

    static void getTestData() {
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        for (integer i = 0; i < 2; i ++) {
            accounts.add(new Account(Name = 'Account ' + i));
        }
        insert accounts;
        for (integer i = 0; i < 5; i ++) {
            contacts.add(
                    new Contact(
                            LastName = 'Contact ' + i + ' of ' + accounts[0].Name,
                            AccountId = accounts[0].Id,
                            Amount__c = 9000
                    )
            );
        }
        for (integer i = 0; i < 5; i ++) {
            contacts.add(
                    new Contact(
                            LastName = 'Contact ' + i + ' of ' + accounts[1].Name,
                            AccountId = accounts[0].Id,
                            Amount__c = 11000
                    )
            );
        }
        insert contacts;
    }
    static void getSwitchTestData() {
        List<Account> accounts = new List<Account>();
        for (integer i = 0; i < 2; i ++) {
            accounts.add(new Account(Name = 'Account ' + i));
        }
        insert  accounts;
        for (integer i = 0; i < 5; i ++) {
            AccountSwitcherImpl.allContacts.add(
                    new PublicContact (
                            new Contact (
                                    LastName = 'Contact ' + i + ' of ' + accounts[0].Name,
                                    AccountId = accounts[0].Id,
                                    Amount__c = 9000
                            ),
                            123456
                    )
            );
        }
        AccountSwitcherImpl.allContacts.add(
                new PrivateContact (
                        new Contact (
                                LastName = 'Contact 0 of ' + accounts[1].Name,
                                AccountId = accounts[1].Id,
                                Amount__c = 11000
                        ),
                        123334
                )
        );
        for (integer i = 1; i < 5; i ++) {
            AccountSwitcherImpl.allContacts.add(
                    new PrivateContact (
                            new Contact (
                                    LastName = 'Contact ' + i + ' of ' + accounts[1].Name,
                                    AccountId = accounts[1].Id,
                                    Amount__c = 11000
                            ),
                            123456
                    )
            );
        }
    }
}