/**
 * Created by User on 1/17/2019.
 */

public interface AccountSwitcher {
    void getAndSort();
    AccountSwitcherImpl.DataResult switchAccount(); 
}